from django.apps import AppConfig


class JosephscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'josephscrumy'
